# ADTF Conan Generator

Header only package to generate a solution to handle dependencies, create a running ADTF environment and deploy your session.

Please have a look at [Release Notes](RELEASENOTES.md)

ADTF treats paths within the local environment with `*.adtfenvironment` files. As this paths are different on each system, they cannot be set during package build and collecting them manually may be a time consuming issue. The `adtf_conan_generator` automizes this and in addtion it creates handy start scripts for common use cases like starting an ADTF project with the ADTF Configuration Editor. A more detailed explanation can be found in the ADTF Guides.

> **Hint**: This documentation is not on how to work with ADTF. For information about ADTF, please have a look at the [Overview Of Products](https://gitlab.com/groups/digitalwerk/solutions/-/wikis/Overview-Of-Products).

## Documentation

Please have a look at our [Guide](doc/html/adtf_conan_generator_guide.md).

## Examples

Examples on easy usage and starting point `conanfile.py`'s are located and listed within [Examples](examples/examples.md).

## License Information

For any licence information have a look at [LICENSE](LICENSE).

Furthermore please refer to [Used Open Source Software](doc/license/3rd_party_licenses.md) for an overview of 3rd party packages we are glad to use.
