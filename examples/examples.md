# Examples

This generator package provides example conanfiles to show the different possibilities of usage.
An user can choose the one which is best suited for an individual use case and can adapt the conanfile to his own needs.
The following conanfiles will be explained regarding their differences and their way to use them.

## General - build the example projects

All examples require a creation of the demo base project. Therefore, copy the example folder from `adtf_conan_generator` conan package to another location (e.g. Desktop). Then, open a command prompt in this folder and execute the following conan commands in adaption to the example you want to build. Initialization of an empty `git` repository is required:

```powershell
git init

cd examples/recipes

conan create conanfile_example_1.py demo_adtf_project_example1/0.0.1@dw/testing
```

## Example 1 - Manual generator call

The first method to call the ADTF Conan Generator is to call it manually by adding the command line option `-g ADTF3Generator` to your `conan install` command. This looks as following:

```powershell
conan install demo_adtf_project_example1/0.0.1@dw/testing -g ADTF3Generator
```

**Preparing the conanfile:**

Let's have a closer look at `conanfile_example_1.py`. The requirement `adtf_conan_generator/1.5.0@dw/stable` is defined in the `requirement` section of the conanfile. Also, there s no need to name the ADTF generator explicitly as `generators="ADTF3Generator"` in the conanfile.

**Behaviour:**

The files are only created if the `-g ADTF3Generator` option is used in the `conan install` call.
The scripts and files will be generated **in the current working directory**!

## Example 2 - Semi automized generator call

The second example shows the mixing of the generator requirement and the explicit naming of the generator in the conanfile.
Also shows you how to override the default launchers, tools, and home elements in new `adtf_configuration_editor.cesettings` file to better meet your needs in the configuration editor

**Preparing the conanfile:**

As mentioned above, the generator requirement and the usage of this generator are present in
the conanfile:

```python
generators="ADTF3Generator"

...

def requirements(self): 
    self.requires("adtf_conan_generator/<version>@dw/stable")
```

**Behaviour:**

This combination leads to the following behaviour:

Creating the package with

```powershell
conan create conanfile_example_2.py demo_adtf_project_example2/0.0.1@dw/testing
```

will create the files and startscripts in the build folder of the package (this is possibly not what has been intended). Installing the created package with

```powershell
conan install demo_adtf_project_example2/0.0.1@dw/testing -g ADTF3Generator
```

will create files and start scripts **in the current working directory**!

## Example 3 - Fully automized (implicit) generator call (experimental!)

The following feature is marked as experimental as it uses the generator in another way as intended by conan.
Normally a user has to call the `-g ADTF3Generator` during install process of the package. The following feature is a workaround for this as it allows to execute the generator by code within your conanfile.

**Preparing the conanfile:**

Having a closer look at `conanfile_example_3.py` reveals some differences, summed up here:

```python
from conans import python_requires
python_requires("adtf_conan_generator/<version>@dw/stable")

...

adtf_generator_implicit = True

...

def package_info(self):
    self.adtf3_projects[0]["TARGET_FOLDER"] = self.package_folder

    ...

    if not self.develop:
        self.python_requires["adtf_conan_generator"].module.ADTF3Generator(self).content
```

The conan requirement is now handled via `python_requires` which is imported from `conans`.
There is no need to name the generator with `generators="ADTF3Generator"`. Also, the tuple `TARGET_FOLDER : "."` of dictionary `adtf3_projects` becomes relevant here in combination with the `package_info()` method. This allows you to define the exact location where the generator is going to create the files and start scripts to. In this configuration it is important to set the `adtf_generator_implicit = True` attribute to make the generator files be written by the generator, **not** by conan as intended by conan.
The implicit generator call happens by actively calling the `content` method of the `adtf_conan_generator` via `python_requires`. To make this happen during `conan install` process only, the develop variable is used (this is not 100% correct, please have a look at the [conan documentation](https://docs.conan.io/en/latest/reference/conanfile/attributes.html?#develop) for further information).

> **Hint**: If the scripts are generated this way in Linux, the files don't have the execution flag set. This has to be done explicitly e.g. in the conanfile after scripts have been generated in the `package_info()` method.

**Behaviour:**

No generator call during package installation is necessary, just use

```powershell
conan install demo_adtf_project_example3/0.0.1@dw/testing
```

The files and start scripts are created in `TARGET_FOLDER`, which is the `self.package_folder` of the package in this example.

> **Hint**: It is also possible to call the `-g ADTF3Generator` during `conan install`. This will additionally create the files and start scripts **in the current working directory**. Therefore it could be useful to set the attribute `adtf_generator_use_absolute_paths` to `True` in the conanfile!

## Example 4 - Transitive packages

If a conan recipe which uses the `adtf_conan_generator` implicitly has a requirement to another conan recipe which also uses the `adtf_conan_generator` implicitly, you have to determine which package is the **root** package.

**Preparing the conanfile:**

In this case the root package is `conanfile_example_4.py` which uses ADTF project `demo_adtf_project_example4`. This package is dependent of `demo_adtf_project_example3/0.0.1@dw/testing` which does also use the `adtf_conan_generator` implicitly. The important point here is the determination that the start scripts and files of `demo_adtf_project_example4` shall **NOT** be created into the package folder of `demo_adtf_project_example3`. Therefore the `adtf_conan_generator` checks the root conanfile for the attribute `adtf3_projects_ignore_list` to determine which project is **not** the root.
For this example it has to be `adtf3_projects_ignore_list = ["demo_adtf_project_example3"]` as we want to install `demo_adtf_project_example4/0.0.1@dw/testing`.

To execute this example, it is necessary to create the packages with

```python
conan create conanfile_example_3.py demo_adtf_project_example3/0.0.1@dw/testing
```

and

```python
conan create conanfile_example_4.py demo_adtf_project_example4/0.0.1@dw/testing
```

After that, `demo_adtf_project_example4/0.0.1@dw/testing` can be installed with

```python
conan install demo_adtf_project_example4/0.0.1@dw/testing
```

**Behaviour:**

If attribute `adtf3_projects_ignore_list` is not set in the conanfile, the start scripts and files of `demo_adtf_project_example4/0.0.1@dw/testing` are generated into the dependency package `demo_adtf_project_example3/0.0.1@dw/testing`, what is probably not intended. Setting the attribute fixes this.
