# Copyright 2024 Digitalwerk GmbH.
#
#     This Source Code Form is subject to the terms of the Mozilla
#     Public License, v. 2.0. If a copy of the MPL was not distributed
#     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# If it is not possible or desirable to put the notice in a particular file, then
# You may include the notice in a location (such as a LICENSE file in a
# relevant directory) where a recipient would be likely to look for such a notice.
#
# You may add additional accurate notices of copyright ownership.

from os.path import join
from conans import __version__ as ConanVersion
from conans.model.version import Version
if ConanVersion < Version("1.19"):
    raise Exception("Conan client version 1.19 or higher required to use this Generator")

from conans import ConanFile

import os

class DemoAdtfProjectExample1(ConanFile):
    name = "demo_adtf_project_example1"
    version = "0.0.1"
    description = "Generates startup scripts and adtfenvironment file for demo_adtf_project"
    url = "https://gitlab.com/digitalwerk/community/conan/adtf_conan_generator.git"
    homepage = "https://www.digitalwerk.net/"
    license = "https://gitlab.com/digitalwerk/community/conan/adtf_conan_generator/LICENSE"

    settings = "os", "compiler", "build_type", "arch"

    adtf_requirement = "adtf/3.15.1@dw/stable"
    adtf_conan_generator_requirement = "adtf_conan_generator/1.5.0@dw/stable"
    
    scm = {
        "type": "git",
        "url": "auto",
        "revision": "auto",
    }
    
    script_dir = os.path.dirname(os.path.realpath(__file__))
    adtf3_projects = [
        {
            "VERSION": version,
            "DESCRIPTION": "ADTF Conan Generator Example Project.",
            "ENVIRONMENT_DIRECTORY_MACRO": "EXAMPLE_PROJECT_DIR",
            "BASE_PATH": os.path.join(script_dir, "example", "demo_adtf_project_base"),
            "PROJECT_PATH": "demo_adtf_project_base.adtfproject",
            "SESSION_PATH": "adtfsessions/demo_adtf_session.adtfsession",
            "START_ARGS": {
                    "ce": "",
                    "launcher": "--console --run",
                    "launcher_with_profiling": "--console --run",
                    "control": "",
                    "guicontrol": ""
                },
            "PLUGIN_DIRECTORIES": {
                "bin",
                "bin/debug"
            },
            "DOCUMENTATION": {
                "Demo ADTF Conan Generator Example Project Documentation": "doc/examples.md"
            },
            "TARGET_FOLDER": "." # not relevant here
        }
    ]
    
    def requirements(self):
        self.requires(self.adtf_requirement)
        self.requires(self.adtf_conan_generator_requirement)

    def package(self):
        self.copy("*.*", 
            src=os.path.join(self.source_folder, "example", "projects", "demo_adtf_project_base"), dst="demo_adtf_project_base")
        self.copy("examples.md", 
            src=os.path.join(self.source_folder, "example"), dst="doc")
        self.copy("conanfile_example_1.py",
            src=os.path.join(self.source_folder, "example", "recipes"), dst=os.path.join(self.package_folder, "example"))

    def package_info(self):
        self.adtf3_projects[0]["BASE_PATH"] = os.path.join(self.package_folder, "demo_adtf_project_base")
        self.user_info.ADTF3_PROJECTS = self.adtf3_projects

    def package_id(self):
        self.info.header_only()
