# Copyright 2024 Digitalwerk GmbH.
#
#     This Source Code Form is subject to the terms of the Mozilla
#     Public License, v. 2.0. If a copy of the MPL was not distributed
#     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# If it is not possible or desirable to put the notice in a particular file, then
# You may include the notice in a location (such as a LICENSE file in a
# relevant directory) where a recipient would be likely to look for such a notice.
#
# You may add additional accurate notices of copyright ownership.

from types import CodeType
from conans import __version__ as ConanVersion
from conans.model.version import Version
if ConanVersion < Version("1.19"):
    raise Exception("Conan client version 1.19 or higher required to use this Generator")

from conans import ConanFile, tools, CMake
from conans.model import Generator
from conans.errors import ConanException

import os
from xml.etree.ElementTree import ElementTree, Element, SubElement, tostring, Comment, parse, register_namespace
from xml.dom import minidom
import pathlib
from os.path import join
from enum import Enum 
import shutil

def check_convert_to_list(param):
    if isinstance(param, list):
        return param
    elif isinstance(param, str):
        return [param]
    else:
        raise ConanException("The parameter is not of type list or string.")

def eval_var(var):
    try:
        return eval(var)
    except:
        return var

class CESettings():

    def override(self, ce_settings_list):
        if len(ce_settings_list) == 0: 
            return
        
        root = self.tree.getroot()
        
        for setting in ce_settings_list:
            if "launchers" in setting:
                launch_settings = root.find('.//{adtf/xsd/cesettings}launch_settings')
                launchers = launch_settings.find('{adtf/xsd/cesettings}launchers')
                for launcher in launchers.findall('{adtf/xsd/cesettings}launcher'):
                    launchers.remove(launcher)
                # Add launchers
                for launcher_info in setting["launchers"]:
                    launcher = SubElement(launchers, 'launcher')
                    name = SubElement(launcher, 'name')
                    name.text = launcher_info["name"]
                    executable = SubElement(launcher, 'executable')
                    executable.text = launcher_info["executable"] #.replace('"',"&quot;")
                    workingdirectory = SubElement(launcher, 'workingdirectory')
                    workingdirectory.text = launcher_info["workingdirectory"]
                    shortcut = SubElement(launcher, 'shortcut')
                    shortcut.text = launcher_info["shortcut"]
                    icon_path = SubElement(launcher, 'icon_path')
                    icon_path.text = launcher_info["icon_path"]

            if "tools" in setting:
                tool_settings = root.find('.//{adtf/xsd/cesettings}tool_settings')
                tools = tool_settings.find('{adtf/xsd/cesettings}tools')
                for tool in tools.findall('{adtf/xsd/cesettings}tool'):
                    tools.remove(tool)

                for tool_info in setting["tools"]:
                    tool = SubElement(tools, 'tool')
                    name = SubElement(tool, 'name')
                    name.text = tool_info["name"]
                    executable = SubElement(tool, 'executable')
                    executable.text = tool_info["executable"] #.replace('"',"&quot;")
                    shortcut = SubElement(tool, 'shortcut')
                    shortcut.text = tool_info["shortcut"]
                    icon_path = SubElement(tool, 'icon_path')
                    icon_path.text = tool_info["icon_path"]
                    arguments = SubElement(tool, "arguments")
                    arguments.text = tool_info["arguments"]    

            if "home_elements" in setting:
                home_settings = root.find('.//{adtf/xsd/cesettings}home_settings')
                home_entries = home_settings.find('{adtf/xsd/cesettings}home_entries')
                for home_element in home_entries.findall('{adtf/xsd/cesettings}home_element'):
                    home_entries.remove(home_element)

                # Add home_elements
                for home_element_info in setting["home_elements"]:
                    home_element = SubElement(home_entries, 'home_element')
                    name = SubElement(home_element, 'name')
                    name.text = home_element_info["name"]
                    page_url = SubElement(home_element, 'page_url')
                    page_url.text = home_element_info["page_url"]
                    icon_path = SubElement(home_element, 'icon_path')
                    icon_path.text = home_element_info["icon_path"]
                    java_script = SubElement(home_element, 'java_script')
                    java_script.text = home_element_info["java_script"]
                    
            if "remote_settings" in setting:
                remote_settings = root.find('.//{adtf/xsd/cesettings}remote_settings')
                if remote_settings is None:
                    continue
                enabled = remote_settings.find('{adtf/xsd/cesettings}enabled')
                if enabled is None:
                    enabled = SubElement(remote_settings, 'enabled')
                fep_discovery = remote_settings.find('{adtf/xsd/cesettings}fep_discovery')
                if fep_discovery is None:
                    fep_discovery = SubElement(remote_settings, 'fep_discovery')
                remotes = remote_settings.find('{adtf/xsd/cesettings}remotes')
                if remotes is None:
                    remotes = SubElement(remote_settings, 'remotes')
                for remote in remotes.findall('{adtf/xsd/cesettings}remote_setting'):
                    remotes.remove(remote)

                # Update remote_settings
                enabled.text = str(setting["remote_settings"]["enabled"]).lower()
                fep_discovery.text = str(setting["remote_settings"]["fep_discovery"]).lower()

                # Add remote settings
                for remote_info in setting["remote_settings"]["remotes"]:
                    remote_setting = SubElement(remotes, 'remote_setting')
                    url = SubElement(remote_setting, 'url')
                    url.text = remote_info["url"]
                    
    def load(self, file):
        if not os.path.exists(file):
            raise Exception("ADTF settings file not found: " + file)
        
        #Prevent unnecessary "ns0" namespace during saving
        register_namespace('', "adtf/xsd/cesettings")
        self.tree = parse(file)

    def generate(self):
        root = self.tree.getroot()

        xml_content = minidom.parseString(tostring(root, encoding='utf-8', xml_declaration=True)).toprettyxml(indent="    ")
        # add encoding to xml header
        xml_content = xml_content.replace('<?xml version=\"1.0\" ?>', '<?xml version="1.0" encoding="UTF-8"?>')

        return xml_content
        
class ADTFFileGeneratorBase():
    def __init__(self, os) -> None:
        self.os = os

class ADTFEnvironmentGenerator(ADTFFileGeneratorBase):

    def __init__(self, os) -> None:
        super().__init__(os)
        self.macros = None
        self.add_plugin_dirs = None
        self.env_dir_macro = None
        self.env_file_descr = None
        self.env_file_version = None
        self.environment_files = None
        self.env_file_doc = None
        self.use_absolute_paths = None
        self.project_name = "NA"

    def set_environment_files(self, environment_files):
        self.environment_files = environment_files

    def set_project_name(self, project_name):
        self.project_name = project_name
        
    def set_macros(self, macros):
        self.macros = macros

    def set_env_dir_macro(self, env_dir_macro):
        self.env_dir_macro = env_dir_macro

    def set_env_file_descr(self, env_file_descr):
        self.env_file_descr = env_file_descr

    def set_add_plugin_dirs(self, add_plugin_dirs):
        self.add_plugin_dirs = add_plugin_dirs

    def set_version(self, env_file_version):
        self.env_file_version = env_file_version

    def set_documentation(self, env_file_doc):
        self.env_file_doc = env_file_doc

    def set_use_absolute_paths(self, use_absolute_paths):
        self.use_absolute_paths = use_absolute_paths
        
    def generate(self):
        root = Element("environment_file", {"xmlns": "adtf/xsd/environment"})
        
        file_version_element = SubElement(root, "file_version")
        major_element = SubElement(file_version_element, "major")
        minor_element = SubElement(file_version_element, "minor")
        major_element.text = "1"
        minor_element.text = "0"
        
        name_element = SubElement(root, "name")
        name_element.text = self.project_name
        
        version_element = SubElement(root, "version")
        version_element.text = self.env_file_version if self.env_file_version else "1.0.0"
        
        platform_element = SubElement(root, "platform")
        if self.os =="Windows":
            platform_element.text = "win64_vc141"
        elif self.os == "Arm":
            platform_element.text = "linux_aarch64"
        else:
            platform_element.text = "linux64"
            
        description_element = SubElement(root, "description")
        description_element.text = self.env_file_descr if self.env_file_descr else "ADTF3 project environment file - generated by the ADTF3 generator"
        
        environment_macro_element = SubElement(root, "environment_directory_macro")
        environment_macro_element.text = self.env_dir_macro if self.env_dir_macro else "GENERATOR_DIR"
        
        includes_element = SubElement(root, "includes")
        
        if self.environment_files:
            for env_file in self.environment_files:
                include_element = SubElement(includes_element, "include")
                url_element = SubElement(include_element, "url")
                url_element.text = env_file.replace("\\", "/")
                #comment = Comment("Dependency: "+)
                #include_element.insert(0,comment)
        
        macros_element = SubElement(root, "macros")

        if self.macros:
            for k, v in self.macros.items():
                macro = SubElement(macros_element, "macro")
                name = SubElement(macro, "name")
                value = SubElement(macro, "value")

                name.text = k
                value.text = v
        
        plugin_dir_element = SubElement(root, "plugin_directories")

        if self.add_plugin_dirs:
            for url_path in self.add_plugin_dirs:
                plugin_dir = SubElement(plugin_dir_element, "plugin_directory")
                url = SubElement(plugin_dir, "url")
                url.text = url_path

        SubElement(root, "ce_plugin_directories")
        
        doc_element = SubElement(root, "documentation")

        if self.env_file_doc:
            for k, v in self.env_file_doc.items():
                document = SubElement(doc_element, "document")
                name = SubElement(document, "name")
                url = SubElement(document, "url")

                name.text = k
                url.text = v
        
        # convert element tree to a formatted string representing the xml content
        xml_content = minidom.parseString(tostring(root, encoding="unicode")).toprettyxml(indent="    ")
        # add encoding to xml header
        xml_content = xml_content.replace('<?xml version=\"1.0\" ?>', '<?xml version="1.0" encoding="UTF-8"?>')
        
        return xml_content

class ADTFTool(Enum):
    LAUNCHER = 1
    GUI_CONTROL = 2
    CONTROL = 3
    CE = 4
    
class ADTFStartScript(ADTFFileGeneratorBase):

    def __init__(self, os):
        super().__init__(os)
        self.ce_settings_file = None

    def set_install_directory(self, directory):
        self.install_directory = directory

    def set_adtf_directory(self, directory):
        self.adtf_directory = directory

    def set_adtf_project(self, adtf_project):
        self.adtf_project = adtf_project

    def set_adtf_environment_file(self, environment_file):
        self.environment_file = environment_file

    def set_adtf_ce_settings_file(self, ce_settings_file):
        self.ce_settings_file = ce_settings_file

    def set_adtf_session_file(self, adtf_session):
        self.adtf_session = adtf_session

    def set_is_debug(self, is_debug):
        self.is_debug = is_debug

    def set_use_absolute_paths(self, use_absolute_paths):
        self.use_absolute_paths = use_absolute_paths

    def get_adtf_bin_path(self, is_debug):
        if is_debug:
            return os.path.join(self.adtf_directory, "bin", "debug").replace("\\", "/")
        else:
            return os.path.join(self.adtf_directory, "bin").replace("\\", "/")

    def get_adtf_tool_bin_path(self, adtf_tool):
        if adtf_tool == ADTFTool.LAUNCHER:
            return self.get_adtf_bin_path(self.is_debug)
        else:
            return self.get_adtf_bin_path(False)

    def generate_launcher_arg(self):
        return "adtf_debug" if self.is_debug else "adtf"

    def generate_start_control(self, gui, params=None):
        '''
        prepares the content of the start scripts for ADTF control and ADTF GUI control
        @param gui: if True, the content of the ADTF GUI control script is prepared; otherwise the content of the ADTF control script
        @param params: additional parameters that should be used 
        @return: the content of the start script 
        '''
        
        path = os.path.join(self.install_directory, self.environment_file).replace("\\", "/") if self.use_absolute_paths else self.environment_file
        adtf_control_call = '"{control_path}" --launch "{session_path}" --environment {env_file} --launcher {launcher} {add_params}'.format( 
            control_path=os.path.join(self.get_adtf_tool_bin_path(ADTFTool.CONTROL), 'adtf_guicontrol' if gui else 'adtf_control').replace("\\", "/"),
            session_path=self.adtf_session.replace("\\", "/"),
            env_file=path,
            launcher=self.generate_launcher_arg(),
            add_params=params or "")
            
        return self.generate_start_script(adtf_control_call)

    def generate_start_launcher(self, params=None):
        '''
        prepares the content of the start scripts for ADTF control and ADTF GUI control
        @param params: additional parameters that should be used 
        @return: the content of the start script 
        '''
        
        path = os.path.join(self.install_directory, self.environment_file).replace("\\", "/") if self.use_absolute_paths else self.environment_file
        adtf_launcher_call = '"{control_path}" --session "{session_path}" --environment {env_file} {add_params}'.format( 
            control_path=os.path.join(self.get_adtf_tool_bin_path(ADTFTool.LAUNCHER), 'adtf_launcher').replace("\\", "/"),
            session_path=self.adtf_session.replace("\\", "/"),
            env_file=path,
            add_params=params or "")
            
        return self.generate_start_script(adtf_launcher_call)

    def generate_start_launcher_with_profiling(self, params=None):
        '''
        prepares the content of the start scripts for ADTF control and ADTF GUI control
        @param params: additional parameters that should be used
        @return: the content of the start script
        '''

        path = os.path.join(self.install_directory, self.environment_file).replace("\\", "/") if self.use_absolute_paths else self.environment_file
        adtf_launcher_call = '"{control_path}" --session "{session_path}" --environment {env_file} {add_params} --profiler --profiler-dump-file "{session_name}.prof"'.format(
            control_path=os.path.join(self.get_adtf_tool_bin_path(ADTFTool.LAUNCHER), 'adtf_launcher').replace("\\", "/"),
            session_path=self.adtf_session.replace("\\", "/"),
            session_name=pathlib.Path(self.adtf_session.replace("\\", "/")).stem,
            env_file=path,
            add_params=params or "")

        return self.generate_start_script(adtf_launcher_call)
        
    def generate_start_script(self, line):
        install_dir = self.install_directory.replace("\\", "/") + "/" if self.use_absolute_paths else self._get_os_current_script_dir()
        
        lines = []
        lines.append(self._get_os_script_shebang())
        lines.append("")
        lines.append("echo calling environment activation")
        lines.append(self._get_os_script_call() + install_dir + "activate" + self._get_os_script_ext())
        lines.append("echo -------------------- starting ADTF 3 configuration with: --------------------")
        lines.append("echo %s" % line)
        lines.append("echo ---------------------------------------------------------------------------")
        lines.append(line)    
        lines.append("echo calling environment deactivation")
        lines.append(self._get_os_script_call() + install_dir + "deactivate" + self._get_os_script_ext())
        lines.append("")    
        lines.append("@echo on" if self.os == "Windows" else "")
        return os.linesep.join(lines)

    def generate_start_ce(self, params = None):
        '''
        prepares the content of the configuration editor start script
        @return: the content of the start script
        '''
        ce_settings_args = ""

        if self.ce_settings_file:
            ce_settings_args = "--settings {settings_file}".format(settings_file=self.ce_settings_file) 
        

        lines = []
        if self.adtf_project:
            path = os.path.join(self.install_directory, self.environment_file).replace("\\", "/") if self.use_absolute_paths else self.environment_file
            adtf_ce_call = '"{ce_path}" --project "{project_path}" --environment {env_file} {ce_settings_args} {add_params}'.format( 
                project_path=self.adtf_project.replace("\\", "/"),
                ce_path=os.path.join(self.get_adtf_tool_bin_path(ADTFTool.CE), 'adtf_configuration_editor').replace("\\", "/"),
                env_file=path,
                ce_settings_args=ce_settings_args,
                add_params=params or "")
        else:
            adtf_ce_call = '"{ce_path}"  {ce_settings_args} {add_params}'.format(
                ce_path=os.path.join(self.get_adtf_tool_bin_path(ADTFTool.CE), 'adtf_configuration_editor').replace("\\", "/"),
                ce_settings_args=ce_settings_args,
                add_params=params or "")
        
        return self.generate_start_script(adtf_ce_call)

    def _get_os_script_call(self):
        if self.os =="Windows":
            return "call "
        else:
            return ". "
        
    def _get_os_script_var_def(self):
        if self.os =="Windows":
            return "set "
        else:
            return "export "
        
    def _get_os_process_start(self):
        if self.os =="Windows":
            return "start /wait "
        else:
            return ""
        
    def _get_os_script_ext(self):
        if self.os =="Windows":
            return ".bat"
        else:
            return ".sh"
        
    def _get_os_script_params(self):
        if self.os =="Windows":
            return " %*"
        else:
            return " $@"
        
    def _get_os_current_script_dir(self):
        if self.os =="Windows":
            return "%~dp0"
        else:
            return "$(dirname $(readlink -f $0))/"
        
    def _get_os_script_shebang(self):
        if self.os =="Windows":
            return "@echo off\nsetlocal"
        else:
            return "#!/bin/bash"
        
    def _get_os_script_exist_execute(self, file_path):
        if self.os =="Windows":
            return "if exist \"" + file_path + "\" ( " + self._get_os_script_call() + file_path + " ) else ( echo file not found )"
        else:
            return "[[ -f \"" + file_path + "\" ]] && " + self._get_os_script_call() + file_path + " || echo file not found"

class ADTFStartScriptsGenerator():
    def __init__(self, project_name, project_path, os) -> None:

        self._project_name = project_name
        self._project_path = project_path
        self._os = os
        self._script_types = {"ce", "control", "guicontrol", "launcher", "launcher_with_profiling"}

        self._environment_file = None
        self._adtf_dir = None
        self._sessions = []
        self.shell_generator = ADTFStartScript(self._os)
        self.adtf_ce_settings_file = None
        pass
    
    def set_adtf_dir(self, adtf_dir):
        self._adtf_dir = adtf_dir

    def set_environment_file(self, environment_file):
        self._environment_file = environment_file

    def set_install_directory(self, install_directory):
        self._install_directory = install_directory

    def add_session(self, session_name, session_path):
        self._sessions.append({'name': session_name, 'path': session_path})
    
    def set_start_args(self, start_args):
        self._start_args = start_args

    def set_is_debug(self, is_debug):
        self._is_debug = is_debug

    def set_use_absolute_paths(self, use_absolute_paths):
        self._use_absolute_paths = use_absolute_paths

    def set_use_project_name(self, use_project_name):
        self.use_project_name = use_project_name
    
    def set_adtf_ce_settings_file(self, adtf_ce_settings_file):

        self.adtf_ce_settings_file = adtf_ce_settings_file

    def __create_script_name(self, prefix, session_name):
        if self.use_project_name:
            return prefix + "_" + self._project_name + "_" + session_name + self.shell_generator._get_os_script_ext()
        else:
            return prefix + "_" + session_name + self.shell_generator._get_os_script_ext()
    
    def generate(self):
        result = {}
        
        if self._environment_file is not None:
            self.shell_generator.set_adtf_directory(self._adtf_dir)
            self.shell_generator.set_install_directory(self._install_directory)
            self.shell_generator.set_adtf_project(self._project_path)
            self.shell_generator.set_adtf_environment_file(self._environment_file)
            self.shell_generator.set_is_debug(self._is_debug)
            self.shell_generator.set_use_absolute_paths(self._use_absolute_paths)
            self.shell_generator.set_adtf_ce_settings_file(self.adtf_ce_settings_file)
            if "ce" in self._start_args:
                result["start_ce_" + self._project_name + self.shell_generator._get_os_script_ext()] = self.shell_generator.generate_start_ce(self._start_args["ce"])

            for session in self._sessions:
                self.shell_generator.set_adtf_session_file(session["path"])
                if "control" in self._start_args:
                    result[self.__create_script_name("start_control", session["name"])] = self.shell_generator.generate_start_control(False, self._start_args["control"])
                if "guicontrol" in self._start_args:
                    result[self.__create_script_name("start_guicontrol", session["name"])] = self.shell_generator.generate_start_control(True, self._start_args["guicontrol"])
                if "launcher" in self._start_args:
                    result[self.__create_script_name("start_launcher", session["name"])] = self.shell_generator.generate_start_launcher(self._start_args["launcher"])
                if "launcher_with_profiling" in self._start_args:
                    result[self.__create_script_name("start_launcher_with_enabled_profiling_live", session["name"])] = self.shell_generator.generate_start_launcher_with_profiling(self._start_args["launcher_with_profiling"])
        return result
    

class ADTF3Generator(Generator):
   
    _generate_2_dir = None
    _ADTF_dep = None
    @property
    def generate_2_dir(self):
        if not self._generate_2_dir:
            self._generate_2_dir = "ADTF3_gen"
        return self._generate_2_dir
        
    @property
    def filename(self):
        pass 

    @property 
    def self_contained(self):
        return os.environ.get("SELF_CONTAINED")
    
    def _get_user_info_list(self, key_word, remove_from_list=[]):
        ui_list = []
        if self.conanfile.user_info and key_word in self.conanfile.user_info.vars:
            entry = eval_var(getattr(self.conanfile.user_info, key_word))
            ui_list.extend(check_convert_to_list(entry))
        for dep, the_vars in self.deps_user_info.items():
            if dep in remove_from_list:
                continue

            if key_word in the_vars.vars:
                entry = eval_var(the_vars.vars[key_word])
                ui_list.extend(check_convert_to_list(entry))
        
        return ui_list
    
    def rel_path(self, path):
        if(self.self_contained):
            return os.path.relpath(path, self.conanfile.install_folder)
        return path
    
    @property
    def ADTF_dep(self):
        if not self._ADTF_dep:
            for dep_name, dep_cpp_info in self.deps_build_info.dependencies:
#                self.conanfile.output.info("Processing: " + dep_name)
                if dep_name == "ADTF" or dep_name == "adtf":
                    version = Version(dep_cpp_info.version)
                    #if (version >= "3.9.0" and version < "4.0.0") or version == "master":
                    self._ADTF_dep = dep_cpp_info
                    #    break
                    #else:
                    #    raise ConanException("Incompatible ADTF version. Compatible versions are 'master' or >= 3.9.0 and < 4.0.0")
                
            if not self._ADTF_dep:
                raise ConanException("There is no suitable ADTF 3 dependency defined as requirement.")
        return self._ADTF_dep


    _ADTF_dir = None
    @property
    def ADTF_dir(self):
        if self.self_contained:
            self._ADTF_dir = os.path.join("dependencies", "adtf").replace("\\", "/")
        elif not self._ADTF_dir:
            self._ADTF_dir = self.ADTF_dep.rootpath.replace("\\", "/")
        return self._ADTF_dir
    
    _is_debug = None
    @property
    def is_debug(self):
        if not self._is_debug:
            self._is_debug = True if self.conanfile.settings.get_safe("build_type") == "Debug" else False
        return self._is_debug

    _use_absolute_paths = None
    @property 
    def use_absolute_paths(self):
        if not self._use_absolute_paths:
            self._use_absolute_paths = True if hasattr(self.conanfile, "adtf_generator_use_absolute_paths") and self.conanfile.adtf_generator_use_absolute_paths else False
        return self._use_absolute_paths


    _ADTF_bin_dir_rel = None
    @property
    def ADTF_bin_dir_rel(self):
        if self.self_contained:
            self._ADTF_bin_dir_rel = os.path.join( "dependencies", "adtf", "bin").replace("\\", "/")
        elif not self._ADTF_bin_dir_rel:
            self._ADTF_bin_dir_rel = os.path.join(self.ADTF_dep.rootpath, "bin").replace("\\", "/")
        return self._ADTF_bin_dir_rel


    _ADTF_environment_files = None
    @property
    def ADTF_environment_files(self):
        self.conanfile.output.info("Dependencies Environment Files:")

        if not self._ADTF_environment_files:
            key_word="ADTF3_ENVIRONMENT_FILES"
            environment_files = []
            dependencies = os.path.join(self.conanfile.install_folder, "dependencies")
            if self.self_contained:
                self.conanfile.output.info("    Selfcontained Package")
                shutil.rmtree(dependencies, True)
                os.mkdir(dependencies)
            
            for dep_name, dep_cpp_info in self.deps_build_info.dependencies:
                dep_environment_files = []
                user_info = self.deps_user_info[dep_name]

                dep_rootpath = dep_cpp_info.rootpath
                self.conanfile.output.info("    Dependency " + str(dep_name) + ":")
                if self.self_contained:
                    dep_rel_rootpath = os.path.join(dependencies, dep_name)
                    self.conanfile.output.info("       copy dependency " + str(dep_rootpath) + " => " + str(dep_rel_rootpath))
                    shutil.copytree(dep_rootpath, dep_rel_rootpath)
                    dep_rootpath = dep_rel_rootpath

                if key_word in user_info.vars:
                    entry = eval_var(user_info.vars[key_word])
                    for file in check_convert_to_list(entry):
                        if(self.self_contained):
                            file = os.path.join(dep_rootpath, os.path.relpath(file, dep_cpp_info.rootpath))
                        dep_environment_files.append(str(file))
                else:
                    # If the recipe doesn't explicitly export an environment file, we must scan the package root for compatibility reasons
                    for file in pathlib.Path(dep_rootpath).glob('*.adtfenvironment'):
                        dep_environment_files.append(str(file))
                    # If there are multiple environment files, we can not guess which ones would had been the correct one
                    # In explicit notion, exporting mutiple environment files is legal though
                    if len(dep_environment_files) >= 2:
                        self.conanfile.output.error("    Dependency " + str(dep_name) + " contains multiple environment files, use user_info.ADTF3_ENVIRONMENT_FILES to explicitly specify entry point!")
                        continue
                
                if self.self_contained:
                    def transform_entry(file):
                        return os.path.relpath(file, self.conanfile.install_folder)

                    # Transformed list using list comprehension
                    dep_environment_files = [transform_entry(file) for file in dep_environment_files]

                if dep_environment_files:
                    for file in dep_environment_files:
                        self.conanfile.output.info("       found environment file " + str(file))
                environment_files.extend(dep_environment_files)

            self._ADTF_environment_files = environment_files
        return self._ADTF_environment_files
    
    
    _ADTF_projects = None
    @property
    def ADTF_projects(self):
        if not self._ADTF_projects:
            self._ADTF_projects = []
            if hasattr(self.conanfile, "adtf3_projects"):
                self._ADTF_projects.extend(self.conanfile.adtf3_projects)
            remove_list = []
            if hasattr(self.conanfile, "adtf3_projects_ignore_list"):
                remove_list.extend(self.conanfile.adtf3_projects_ignore_list)
            key_word="ADTF3_PROJECTS"
            self._ADTF_projects.extend(self._get_user_info_list(key_word, remove_list))
        return self._ADTF_projects

    _ADTF_ce_settings = None
    @property
    def ADTF_ce_settings(self):
        if not self._ADTF_ce_settings:
            self._ADTF_ce_settings = []
            if hasattr(self.conanfile, "adtf3_ce_settings"):
                self._ADTF_ce_settings.append(self.conanfile.adtf3_ce_settings)
            remove_list = []
            if hasattr(self.conanfile, "adtf3_projects_ignore_list"):
                remove_list.append(self.conanfile.adtf3_projects_ignore_list)
            key_word="ADTF3_CE_SETTINGS"
            self._ADTF_ce_settings.append(self._get_user_info_list(key_word, remove_list))
        return self._ADTF_ce_settings
    
    @property
    def content(self):
        '''
        @return: a dict of the form {<filename>: <content>, ...} containing all files that will be generated
        '''
        
        result = {}
        projects_dicts = self.ADTF_projects
       
        # append a None project to get a universal startup script
        projects_dicts.append(None)

        if self.conanfile.settings.os =="Windows":
            os_text = "Windows"
        elif self.conanfile.settings.os == "Linux" and self.conanfile.settings.arch == "armv8":
            os_text = "Arm"
        else:
            os_text = "Linux"

        # get all environment files from dependencies

        environment_files =  self.ADTF_environment_files

        # get all settings from project
        for project_dict in projects_dicts:
            if project_dict is None:
                continue
            
            base_path = (project_dict.get("BASE_PATH") if project_dict else None)
            # Check SOURCE_SUBFOLDER to be compatible to old version
            if(base_path == None):
                base_path = (project_dict.get("SOURCE_SUBFOLDER") if project_dict else None)
            project_path = (project_dict.get("PROJECT_PATH") if project_dict else None)
            session_path = (project_dict.get("SESSION_PATH") if project_dict else None)
            start_args = (project_dict.get("START_ARGS") if project_dict else None)
            use_project_name = (project_dict.get("USE_PROJECT_NAME") if project_dict else True)    
            macros = (project_dict.get("MACROS") if project_dict else None)    
            env_dir_macro = (project_dict.get("ENVIRONMENT_DIRECTORY_MACRO") if project_dict else None)
            target_dir = (project_dict.get("TARGET_FOLDER") if project_dict else None)
            env_file_descr = (project_dict.get("DESCRIPTION") if project_dict else None)
            add_plugin_dirs = (project_dict.get("PLUGIN_DIRECTORIES") if project_dict else None)
            env_file_version = (project_dict.get("VERSION") if project_dict else None)
            env_file_doc = (project_dict.get("DOCUMENTATION") if project_dict else None)

            if(self.self_contained):
                new_project_dir = os.path.join("project")
                shutil.rmtree(new_project_dir, True)
                self.conanfile.output.info("    Copy Project " + str(base_path) + " to " + new_project_dir)

                def ignore_git(src, names):
                    ignored = set()
                    for name in names:
                        if name == '.git':
                            ignored.add(name)
                    return ignored
                
                shutil.copytree(base_path, new_project_dir, ignore=ignore_git)
                base_path = new_project_dir
                

            if start_args == None:
                start_args = {
                    "ce": "", 
                    "launcher": "",
                    "launcher_with_profiling": "",
                    "control": "",
                    "guicontrol": ""}

            additional_environment_files = (project_dict.get("ADDITIONAL_ENVIRONMENT_FILES") if project_dict else [])
            
            if additional_environment_files is None:
                additional_environment_files = []
            elif(type(additional_environment_files) is not list):
                additional_environment_files = [additional_environment_files]

            if project_path == None:
                self.conanfile.output.warn("Project sciped because of PROJECT_PATH is missing")
                continue

            project_name = (os.path.basename(project_path).replace(".adtfproject", "")) if project_path else ""
            self.conanfile.output.info("Found Project " + project_name) 
            
            ###############################
            # generate adtf environment files for project
            ###############################
            overall_environment_file = project_name + ".adtfenvironment"
            
            if project_dict:
                environment_file = ADTFEnvironmentGenerator(os_text)
                environment_file.set_project_name(project_name)
                environment_file.set_environment_files(environment_files + additional_environment_files)
                if macros:
                    environment_file.set_macros(macros)
                if env_dir_macro:
                    environment_file.set_env_dir_macro(env_dir_macro)
                if env_file_descr:
                    environment_file.set_env_file_descr(env_file_descr)
                if add_plugin_dirs:
                    environment_file.set_add_plugin_dirs(add_plugin_dirs)
                if env_file_version:
                    environment_file.set_version(env_file_version)
                if env_file_doc:
                    environment_file.set_documentation(env_file_doc)
                result[overall_environment_file] = environment_file.generate()
                
            ###############################
            # generate start scripts
            ###############################
            start_script_generator = ADTFStartScriptsGenerator(project_name, self.rel_path(os.path.join(base_path, project_path)), os_text)
            start_script_generator.set_environment_file(overall_environment_file)
            start_script_generator.set_adtf_dir(self.ADTF_dir)
            start_script_generator.set_use_project_name(use_project_name)
            start_script_generator.set_start_args(start_args)
            start_script_generator.set_is_debug(self.is_debug)
            start_script_generator.set_use_absolute_paths(self.use_absolute_paths)
            output_dir = target_dir if target_dir else self.conanfile.install_folder
            start_script_generator.set_install_directory(output_dir)

            if len(self.ADTF_ce_settings) > 0:
                start_script_generator.set_adtf_ce_settings_file("adtf_configuration_editor.cesettings")
                ce_settings = CESettings()
        
                ce_settings.load(os.path.join(self.ADTF_dir, "settings", "adtf_configuration_editor.cesettings"))
                ce_settings.override(self.ADTF_ce_settings)                
                result["adtf_configuration_editor.cesettings"] = ce_settings.generate()

            if session_path:
                name = pathlib.Path(session_path).stem
                start_script_generator.add_session(name, self.rel_path(os.path.join(base_path, session_path)))

            result.update(start_script_generator.generate())
                    
        ###############################
        # if there is no virtualenv generator given, it has to be added and called
        # (the activate and deactivate scripts are required)
        ###############################
        if not "virtualenv" in self.conanfile.generators:
            from conans.client.generators.virtualenv import VirtualEnvGenerator
            veg = VirtualEnvGenerator(self.conanfile)
            veg.output_path = output_dir
            result.update(veg.content)

        ###############################
        # if the installation is not executed by conan the 
        # generator has to write the files manually
        ###############################
        if hasattr(self.conanfile, "adtf_generator_implicit"):
            if self.conanfile.adtf_generator_implicit:
                for filename in result:
                    with open(os.path.join(output_dir, filename), 'w') as fd:
                        fd.writelines(result[filename])
                    print(f"Generator ADTF3Generator created {filename}")
        
        def remove_file(name):
            path = os.path.join(self.conanfile.install_folder, name)
            if os.path.exists(path):
                os.remove(path)

        remove_file("conan.lock")
        remove_file("conanbuildinfo.txt")
        remove_file("conaninfo.txt")

        return result

class Deploy(ConanFile):
    name = "adtf_conan_generator"
    description = "Header only package to generate startup scripts and adtfenvironment file"
    url = "https://gitlab.com/digitalwerk/solutions/conan/adtf_conan_generator.git"
    homepage = "https://gitlab.com/digitalwerk/solutions/conan/adtf_conan_generator"
    license = "MPL-2.0"
    settings = {}

    short_paths = True

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        pass
