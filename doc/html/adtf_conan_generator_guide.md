# Guide

This documentation contains an overview about available settings for the generated files and scripts that can be configured in the `conanfile.py` of the package. For examples on how to use the information listed here, please have a look at the examples located in [Examples](../../examples/examples.md).

## List of generated files and scripts

The following table gives an overview about the generated files which are created by the ADTF Conan Generator:

| File | Description |
|---|---|
| activate[.bat\|.ps1\|.sh] | Activates virtual conan environment.|
| deactivate[.bat\|.ps1\|.sh] | Deactivates virtual conan environment. |
| environment[.bat\|.ps1\|.sh] | Specifies conan environment variables. |
| `<name_of_project_dir>`.adtfenvironment | Dynamic generated `.adtfenvironment` file containing paths collected for the system the generator has been executed on. |
| start_ce_`<project_name>`[.bat\|.sh] | Starts the ADTF Configuration Editor with the specified `.adtfsession` file and arguments. |
| start_launcher_`<project_name>`[.bat\|.sh] | Starts the ADTF Launcher with the specified `.adtfsession` file and arguments. |
| start_launcher_with_enabled_profiling_live_`<project_name>`[.bat\|.sh] | Starts the ADTF Launcher with the specified `.adtfsession` file and arguments. Live profiling is activated. The profiling results are written to the `<session_name>.prof` file in the current folder. |
| start_control_`<project_name>`[.bat\|.sh] | Start the ADTF Control with the specified `.adtfsession` file and arguments. |
| start_guicontrol_`<project_name>`[.bat\|.sh] | Starts the ADTF GUI Control with the specified `.adtfsession` file and arguments. |

## Preparing the Conanfile

The generator can be configured within the conanfile of the project that uses the `adtf_conan_generator`. The following sections show the possibilites how to do this:

### Optional attributes

The following attributes allow the generator to have specific behaviour if the attribute is set to `True`.
If the attributes are not present in the conanfile it is equivalent as setting them to `False`.

| Name | type | description |
|---|---|---|
| adtf_generator_implicit | bool | Set to `True`, if the generator shall be called implicitly. |
| adtf_generator_use_absolute_paths | bool | Set to `True`, if paths in start scripts shall be absolute. This allows to move the generated start scripts to other directories on hard disk. |
| adtf3_projects_ignore_list | dictionary | Determines which transitive projects shall be ignored by the generator. For further information see [example 4](../../examples/examples.md). |
| adtf3_ce_settings | dictionary | allows you to generate a new adtf_configuration_editor.cesettings file. For further information see FAQ and [example 2](../../examples/examples.md). |

### Dictionary

The main configuration of the ADTF Conan Generator can be done via `key-value` entries in the `adtf3_projects` dictionary within the `conanfile.py`. The following table lists all possible entries which are evaluated during file and script generation:

| Entry | Relates to | Description |
|-------|------------|-------------|
| VERSION | adtfenvironment file | Sets product version in the adtfenvironment file. If not given, default version `3.0.0` will be set. |
| DESCRIPTION | adtfenvironment file | Sets a description in the adtfenvironment file. If not given, a default string is used. |
| ENVIRONMENT_DIRECTORY_MACRO | adtfenvironment file | Sets the name of the macro which is used by ADTF to determine the root directory of the created package. If not given, `GENERATOR_DIR` will be used. |
| BASE_PATH | scripts | Determines the **absolute** path to your ADTF project for which the files should be generated. |
| PROJECT_PATH | scripts | Determines path to `.adtfproject` file of the ADTF project **relative** to `BASE_PATH`. |
| SESSION_PATH | scripts | Determines path to `.adtfsession` file of the ADTF project **relative** to `BASE_PATH`. |
| START_ARGS | scripts | Determines which start scripts shall be created and allows to define arguments for them (optional, see below!). |
| (START_ARGS) ce | scripts | If set, a start script for launching the session with the ADTF Configuration Editor will be generated. For a list of [available commands](https://support.digitalwerk.net/adtf/v3/guides/tools_adtf_configuration_editor.html) have a look at the ADTF guides. |
| (START_ARGS) launcher | scripts | If set, a start script for launching the session with the ADTF Launcher will be generated. For a list of [available commands](https://support.digitalwerk.net/adtf/v3/guides/tools_adtf_launcher.html) have a look at the ADTF guides. |
| (START_ARGS) control | scripts | If set, a start script for launching the session with the ADTF Control will be generated. For a list of [available commands](https://support.digitalwerk.net/adtf/v3/guides/tools_adtf_control.html) have a look at the ADTF guides. |
| (START_ARGS) guicontrol | scripts | If set, a start script for launching the session with the ADTF GUI Control will be generated. For a list of [available commands](https://support.digitalwerk.net/adtf/v3/guides/tools_adtf_guicontrol.html) have a look at the ADTF guides. |
| PLUGIN_DIRECTORIES | adtfenvironment file | Comma separated directories **relative** to `BASE_PATH` where to look for `.adtfplugin` files. If not given, `bin` and `bin/debug` will be set as default. |
| DOCUMENTATION | adtfenvironment file | Determines the **relative** path to the documentation with the conan package folder as root directory. |
| TARGET_FOLDER | all generated files | Relevant in combination with `adtf_generator_implicit`. Determines where to generate the files and scripts in **relative** to the conan package root directory. |
| USE_PROJECT_NAME | scripts | If set to `True`, the project name is used instead of the `adtfsession` name. |
| MACROS | adtfenvironment file | Sets macros in the `adtfenvironment` file. |
| SOURCE_SUBFOLDER | scripts | Determines the subfolder if `BASE_PATH` is not set. |

### Self Contained

To create a self-contained package, the environment variable SELF_CONTAINED can be set before invoking the Conan generator. The Conan generator now copies all dependencies alongside the startup script (into the dependencies folder), allowing execution without needing to use the Conan cache. This is particularly useful, for example, when creating a USB stick that is independent of the current PC.

## FAQ's

Here are some topics and use cases which might be helpful:

### How can I define more than one 'adtfsession' within a conanfile ?

The ADTF Conan Generator can be used for more than one `adtfsession` in the same `conanfile.py`. Therefore the `adtf3_projects` dictionary can be extended by a second (or more ..) group of entries. This could look like this:

```python
adtf3_projects = [
    {
        "VERSION": "0.0.1", # Version for adtfenvironment file 1
        "PROJECT_PATH": "adtf_project1.adtfproject", # Path to project 1
        ...
    },
    {
        "VERSION": "other version", # Version for adtfenvironment file 2
        "PROJECT_PATH": "adtf_project2.adtfproject", # Path to project 2
        ...
    },
    ...
]
```

The different session configurations can be referenced in the `package_info()` method of the conanfile by indexing them e.g. like

```python
def package_info(self):
    self.adtf3_projects[0]["VERSION"] = ... # Indexing adtfsession 1
    self.adtf3_projects[1]["VERSION"] = ... # Indexing adtfsession 2
```

### Can I call the generated start scripts with additional arguments ?

The generated start scripts can be made configurable by manipulating the strings that are passed and defined in `adtf3_projects`. For example, if you want to make the `--run` argument configurable later you can use a parameter accessor in dependence of your operating system:

```python
scripts_param_accessor = "$" if os.name == "posix" else "%" # Determine the param accessor 
adtf3_projects = [
        {
        ...
        "PROJECT_PATH": "demo_adtf_project_base.adtfproject",
        "START_ARGS": {
                "launcher": "--control-url " + scripts_param_accessor + "1"
            },
        ...
    }
]
```

Now the start script `start_launcher_demo_adtf_project_base` can be called in Windows e.g. with

```powershell
.\start_launcher_demo_adtf_project_base.bat "--run"
```

## How to use and define macros to be generated ?

Usage:

```python
adtf3_projects = [
     {
        "BASE_PATH": script_dir,
        "PROJECT_PATH": "demo_adtf_project.adtfproject",
        "SESSION_PATH": "adtfsessions/demo_adtf_session.adtfsession",
        "START_ARGS": {
                "ce": "", 
                "launcher": "--run", 
                "control": "",
                "guicontrol": ""},
        "MACROS": {
                "my_macro_1" : "my_value_1",
                "my_macro_2" : "my_value_2"
        }
    }
]
```

Creates:

```xml
...
<macros>
    <macro>
        <name>my_macro_1</name>
        <value>my_value_1</value>
    </macro>
    <macro>
        <name>my_macro_2</name>
        <value>my_value_2</value>
    </macro>
</macros>
...
```

## How to create and share a new ce settings file

The variable `adtf3_ce_settings` allows you to generate a new adtf_configuration_editor.cesettings file, enabling you to override the default launchers, tools, and home elements to better meet your needs in the configuration editor.

The adtf3_ce_settings variable is a dictionary with this main keys:
1. 'launchers': A list of launcher configurations.
2. 'tools': A list of all tools in the tools menu.
3. 'home_elements': A list of entries in the home view
4. 'remote_settings': A list of all remote connection urls
```python
    adtf3_ce_settings = {
        "launchers": [
            {
                "name": "Launch with enabled profiling",
                "executable": 'cmd.exe /S /K " "$(ADTF_DIR)/bin/adtf_launcher" --environment "$(ADTF_ENVIRONMENT_FILENAME)" --session "$(ADTF_SESSION_FILENAME)" --console --run --profiler "',
                "workingdirectory": '$(ADTF_DIR)/bin',
                "shortcut": 'Shift+F5',
                "icon_path": '$(ADTF_DIR)/settings/LauncherProfiling.svg'
            }
        ],
        "tools": [
            {
                "name": "Mapping Editor",
                "executable": '$(ADTF_DIR)/bin/mapping_editor.exe',
                "arguments": "",
                "shortcut": 'Ctrl+Alt+M',
                "icon_path": '$(ADTF_DIR)/settings/mapping_editor.ico'
            },
            {
                "name": "Profiler GUI",
                "executable": '$(ADTF_DIR)/bin/profiler_gui.exe',
                "arguments": "",
                "shortcut": 'Ctrl+Alt+P',
                "icon_path": '$(ADTF_DIR)/settings/profiler_gui.ico'
            }
        ]
        "home_elements": [
            {
                "name": "FAQ",
                "page_url": "https://support.digitalwerk.net/projects/download-center/wiki/FAQ's",
                "icon_path": "qrc:/images/FAQ.svg",
                "java_script": "document.getElementById('top-menu').style.display = 'None'; document.getElementById('header').style.display = 'None'; document.getElementById('main').style.padding = '0'; document.getElementById('sidebar').style.display = 'None'; document.getElementById('content').style.width = '100%';"
            },
            {
                "name": "Feedback",
                "page_url": "https://support.digitalwerk.net/projects/support/issues/new",
                "icon_path": "qrc:/images/Feedback.svg",
                "java_script": ""
            }
        ],
        "remote_settings": {
            "enabled": True,
            "fep_discovery": True,
            "remotes": [
                {"url": "http://localhost:8000"}
            ]
        }
    }
```