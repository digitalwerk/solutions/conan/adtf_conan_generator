# Used Open Source Software

For creation of our artifacts we make use of several 3rd party content.

## Build Tools

Following build tools helps us to compile, document and package our delivery:

|Libraries|Version|License|Homepage|Repository|
|-|-|-|-|-|
|cmake|3.23.2|BSD-3-Clause|[link](https://cmake.org/)|[link](https://gitlab.kitware.com/cmake/cmake)|
|conan|1.59.0|MIT|[link](https://conan.io/)|[link](https://github.com/conan-io/conan)|
|doxygen|1.9.1|GPL-2.0|[link](https://www.doxygen.nl/index.html)|[link](https://github.com/doxygen/doxygen.git)|
|graphviz|2.47.3|EPL-v10|[link](https://graphviz.org/)|[link](https://gitlab.com/graphviz/graphviz)|
